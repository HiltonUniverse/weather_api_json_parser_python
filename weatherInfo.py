
import json
import requests


#Get cityName from user
print("\n")	
ctName = input("Name \"City\" to fetch Weather Information about : ")
print("\n")	

url = "http://api.openweathermap.org/data/2.5/weather?q="+ctName+"&APPID=fe37c3d28a6ebb417f52c05d92b28383"
response = requests.get(url)

if response.status_code == 200:

	print("Featching Weather Information")
	print("-----------------------------")

	jData = json.loads(response.content)

    #Get the Basic Information
	countryName = jData['sys']['country']
	cityName = jData['name']
	sunriseTime = jData['sys']['sunrise']
	sunsetTime = jData['sys']['sunset']
	print("WEATHER INFORMATION OF: " + (countryName)+", " +(cityName))

	#Get the description of the weather
	for feel in jData['weather']:
		print("WEATHER DESCRIPTION:	" +  feel['description'])

	#Get temperature,humidity,pressure
	currentTemp = jData['main']['temp']
	minTemp = jData['main']['temp_min']
	maxTemp = jData['main']['temp_max']
	currentHumidity = jData['main']['humidity']
	currentPressure = jData['main']['pressure']

	#print selected results
	print("CURENT TEMPERATURE IS:  %.2f" % (currentTemp-273) + "°C")	
	print("MIM TEPMERATURE TODAY: 	%.2f" % (minTemp-273) + "°C")	
	print("MAX TEMPERATURE TODAY: 	%.2f" % (maxTemp-273) + "°C")	
	print("CURRENT HUMIDITY IS: 	%.1f" % (currentHumidity)+ "%")
	print("\n")
elif response.status_code == 404:
	print("HTTP 404: Invalid City Name\n")
